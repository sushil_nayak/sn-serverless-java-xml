package com.nayak;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@Builder
public class Response {

	private int statusCode;
	private Map<String,String> headers;
	private String body;
}


package com.nayak;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<Map<String, Object>, Response> {

	private static final Logger LOG = Logger.getLogger(Handler.class);

	@Override
	public Response handleRequest(Map<String, Object> input, Context context) {
		BasicConfigurator.configure();

		LOG.info("received: " + input);

		Map<String, String> map = new HashMap<>();
		map.put("Content-Type", "text/xml");
		return new Response(200,
				map,
				"<root>" + input.get("body") + "<b><a>1</a><c>3</c></b>" + "</root>");
	}

}

// Response(statusCode=200, headers={Content-Type=text/xml}, body=<b><a>1</a><c>3</c></b>)